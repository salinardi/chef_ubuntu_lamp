#
# Cookbook:: initpc
# Recipe:: default
#
# Copyright:: 2018, The Authors, All Rights Reserved.

# *********** UPDATE PACKAGE UBUNTU *************
bash 'inline script' do
        user 'root'
        code 'sudo apt-get update'
end

# *********** INSTALL PACKAGES *************
#package 'emacs'
package 'openssh-server'


# *********** RECIPES *************
include_recipe "chef_ubuntu_lamp::apache"
include_recipe "chef_ubuntu_lamp::php"
include_recipe "chef_ubuntu_lamp::mysql"

