#
# Cookbook:: initpc
# Recipe:: apache
#
# Copyright:: 2018, The Authors, All Rights Reserved.

package 'apache2'

package 'apache2-utils'

# *********** WELCOME INDEX.PHP PAGE *************
cookbook_file '/var/www/html/index.php' do
    source 'apache2/index.php'
end

# *********** APACHE2.CONF FILE *************
cookbook_file '/etc/apache2/apache2.conf' do
	source 'apache2/apache2.conf'
end

bash 'inline script' do
        user 'root'
        code <<-EOH
        touch /etc/apache2/sites-enabled/magento.local.conf
        EOH
end

# *********** VIRTUAL HOSTS FILE *************
cookbook_file '/etc/apache2/sites-enabled/magento.local.conf' do
        source 'apache2/magento.local.conf'
end

# *********** ENVIRONMRNT FOR MAGENTO VIRTUAL HOST *************
# - creates folders for contain site
# - changes permission for the folders
# - creates symbolic link for site-enables
# - adds  127.0.0.1   magento to hosts file
bash 'inline script' do
        user 'root'
        code <<-EOH
        mkdir -p /var/www/magento.local/public_html
        chmod -R 0755 /var/www/
	    a2ensite magento.local
        echo '127.0.0.1   magento.local' >> /etc/hosts
        EOH
end
  
# *********** WELCOME INDEX.PHP PAGE FOR MAGENTO HOST *************
cookbook_file '/var/www/magento.local/public_html/index.php' do
	source 'magento/index.php'
end

# *********** START APACHE *************
service 'apache2' do
	action [ :enable, :start ]
end

