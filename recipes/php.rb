#
# Cookbook:: initpc
# Recipe:: php
#
# Copyright:: 2018, The Authors, All Rights Reserved.

package 'php'
package 'libapache2-mod-php'
package 'php-mcrypt'
package 'php-mysql'
package 'php-xdebug'

# *********** PHP.INI FILE *************
cookbook_file '/etc/php/7.0/apache2/php.ini' do
	source 'php/php.ini'
end
